package karma.pool.util.clock;


import java.util.concurrent.TimeUnit;

public class NanoSecondClock implements Clock {

   @Override
   public long currentTime() {
      return System.nanoTime();
   }

   @Override
   public long toMillis(final long time) {
      return TimeUnit.NANOSECONDS.toMillis(time);
   }


   @Override
   public long toNanos(final long time) {
      return time;
   }

   @Override
   public long elapsedMillis(final long startTime) {
      return TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime);
   }


   @Override
   public long elapsedMillis(final long startTime, final long endTime) {
      return TimeUnit.NANOSECONDS.toMillis(endTime - startTime);
   }


   @Override
   public long elapsedNanos(final long startTime) {
      return System.nanoTime() - startTime;
   }


   @Override
   public long elapsedNanos(final long startTime, final long endTime) {
      return endTime - startTime;
   }


   @Override
   public long plusMillis(final long time, final long millis) {
      return time + TimeUnit.MILLISECONDS.toNanos(millis);
   }


}
