package karma.pool.util.clock;


import java.util.concurrent.TimeUnit;

public final class MilliSecondClock implements Clock {

   @Override
   public long currentTime() {
      return System.currentTimeMillis();
   }


   @Override
   public long elapsedMillis(final long startTime) {
      return System.currentTimeMillis() - startTime;
   }

   @Override
   public long elapsedMillis(final long startTime, final long endTime) {
      return endTime - startTime;
   }


   @Override
   public long elapsedNanos(final long startTime) {
      return TimeUnit.MILLISECONDS.toNanos(System.currentTimeMillis() - startTime);
   }


   @Override
   public long elapsedNanos(final long startTime, final long endTime) {
      return TimeUnit.MILLISECONDS.toNanos(endTime - startTime);
   }


   @Override
   public long toMillis(final long time) {
      return time;
   }

   @Override
   public long toNanos(final long time) {
      return TimeUnit.MILLISECONDS.toNanos(time);
   }


   @Override
   public long plusMillis(final long time, final long millis) {
      return time + millis;
   }


}
