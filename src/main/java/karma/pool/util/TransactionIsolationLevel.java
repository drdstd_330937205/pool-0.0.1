package karma.pool.util;

import lombok.Getter;
import lombok.Setter;

public enum TransactionIsolationLevel {

   none(0),
   read_uncommitted(1),
   read_committed(2),
   repeatable_read(4),
   serializable(8);

   @Setter
   @Getter
   private final int levelId;

   TransactionIsolationLevel(int levelId) {
      this.levelId = levelId;
   }
}
