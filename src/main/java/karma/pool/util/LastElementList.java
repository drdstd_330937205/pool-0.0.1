package karma.pool.util;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * operate last index in this list,and  without range checking.
 */
public final class LastElementList<Element> implements List<Element>, RandomAccess, Serializable {
   private final Class<?> clazz;
   private Element[] elements;
   private int size;


   /**
    * capacity is 32
    */
   public LastElementList(Class<?> clazz) {
      this.elements = (Element[]) Array.newInstance(clazz, 32);
      this.clazz = clazz;
   }

   public LastElementList(Class<?> clazz, int capacity) {
      this.elements = (Element[]) Array.newInstance(clazz, capacity);
      this.clazz = clazz;
   }

   /**
    * Add an element to the tail of the LastElementList.
    *
    * @param element the element to add
    */
   @Override
   public boolean add(Element element) {
      if (size < elements.length) {
         elements[size++] = element;
      } else {
         // overflow-conscious code
         final int oldCapacity = elements.length;
         final int newCapacity = oldCapacity << 1;
         @SuppressWarnings("unchecked") final Element[] newElementData = (Element[]) Array.newInstance(clazz, newCapacity);
         System.arraycopy(elements, 0, newElementData, 0, oldCapacity);
         newElementData[size++] = element;
         elements = newElementData;
      }

      return true;
   }

   /**
    * Get the element at the specified index.
    *
    * @param index the index of the element to get
    * @return the element, or ArrayIndexOutOfBounds is thrown if the index is invalid
    */
   @Override
   public Element get(int index) {
      return elements[index];
   }

   /**
    * Remove the last element from the list.  No bound check is performed, so if this
    * method is called on an empty list and ArrayIndexOutOfBounds exception will be
    * thrown.
    *
    * @return the last element of the list
    */
   public Element removeLast() {
      Element element = elements[--size];
      elements[size] = null;
      return element;
   }

   /**
    * This remove method is most efficient when the element being removed
    * is the last element.  Equality is identity based, not equals() based.
    * Only the first matching element is removed.
    *
    * @param element the element to remove
    */
   @Override
   public boolean remove(Object element) {
      for (int index = size - 1; index >= 0; index--) {
         if (element == elements[index]) {
            final int numMoved = size - index - 1;
            if (numMoved > 0) {
               System.arraycopy(elements, index + 1, elements, index, numMoved);
            }
            elements[--size] = null;
            return true;
         }
      }

      return false;
   }

   /**
    * Clear the LastElementList.
    */
   @Override
   public void clear() {
      for (int i = 0; i < size; i++) {
         elements[i] = null;
      }

      size = 0;
   }

   /**
    * Get the current number of elements in the LastElementList.
    *
    * @return the number of current elements
    */
   @Override
   public int size() {
      return size;
   }


   @Override
   public boolean isEmpty() {
      return size == 0;
   }


   @Override
   public Element set(int index, Element element) {
      Element old = elements[index];
      elements[index] = element;
      return old;
   }


   @Override
   public Element remove(int index) {
      if (size == 0) {
         return null;
      }

      final Element old = elements[index];

      final int numMoved = size - index - 1;
      if (numMoved > 0) {
         System.arraycopy(elements, index + 1, elements, index, numMoved);
      }

      elements[--size] = null;

      return old;
   }


   @Override
   public boolean contains(Object o) {
      throw new UnsupportedOperationException();
   }


   @Override
   public Iterator<Element> iterator() {
      return new Iterator<Element>() {
         private int index;

         @Override
         public boolean hasNext() {
            return index < size;
         }

         @Override
         public Element next() {
            if (index < size) {
               return elements[index++];
            }

            throw new NoSuchElementException("No more elements in LastElementList");
         }
      };
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Object[] toArray() {
      throw new UnsupportedOperationException();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public <E> E[] toArray(E[] a) {
      throw new UnsupportedOperationException();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean containsAll(Collection<?> c) {
      throw new UnsupportedOperationException();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean addAll(Collection<? extends Element> c) {
      throw new UnsupportedOperationException();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean addAll(int index, Collection<? extends Element> c) {
      throw new UnsupportedOperationException();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean removeAll(Collection<?> c) {
      throw new UnsupportedOperationException();
   }


   @Override
   public boolean retainAll(Collection<?> c) {
      throw new UnsupportedOperationException();
   }


   @Override
   public void add(int index, Element element) {
      throw new UnsupportedOperationException();
   }

   @Override
   public int indexOf(Object o) {
      throw new UnsupportedOperationException();
   }


   @Override
   public int lastIndexOf(Object o) {
      throw new UnsupportedOperationException();
   }


   @Override
   public ListIterator<Element> listIterator() {
      throw new UnsupportedOperationException();
   }


   @Override
   public ListIterator<Element> listIterator(int index) {
      throw new UnsupportedOperationException();
   }


   @Override
   public List<Element> subList(int fromIndex, int toIndex) {
      throw new UnsupportedOperationException();
   }


   @Override
   public Object clone() {
      throw new UnsupportedOperationException();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void forEach(Consumer<? super Element> action) {
      throw new UnsupportedOperationException();
   }


   @Override
   public Spliterator<Element> spliterator() {
      throw new UnsupportedOperationException();
   }


   @Override
   public boolean removeIf(Predicate<? super Element> filter) {
      throw new UnsupportedOperationException();
   }

   @Override
   public void replaceAll(UnaryOperator<Element> operator) {
      throw new UnsupportedOperationException();
   }


   @Override
   public void sort(Comparator<? super Element> c) {
      throw new UnsupportedOperationException();
   }
}
