package karma.pool.util;

public interface IBagEntry {
   int state_not_used = 0;
   int state_using = 1;
   int state_removed = -1;
   int state_reserved = -2;

   boolean compareAndSet(int expectState, int newState);

   int getState();

   void setState(int newState);
}
