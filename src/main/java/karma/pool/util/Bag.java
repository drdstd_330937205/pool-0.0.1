package karma.pool.util;

import lombok.extern.slf4j.Slf4j;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.lang.Thread.yield;
import static java.util.concurrent.TimeUnit.MICROSECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static java.util.concurrent.locks.LockSupport.parkNanos;
import static karma.pool.util.IBagEntry.*;
import static karma.pool.util.clock.ClockFactory.currentTime;
import static karma.pool.util.clock.ClockFactory.elapsedNanos;

/**
 * This is a specialized concurrent bag that achieves superior performance
 * to LinkedBlockingQueue and LinkedTransferQueue for the purposes of a
 * proxyConnection pool.  It uses ThreadLocal storage when possible to avoid
 * locks, but resorts to scanning a common collection if there are no
 * available items in the ThreadLocal list.  Not-in-use items in the
 * ThreadLocal lists can be "stolen" when the borrowing thread has none
 * of its own.  It is a "lock-less" implementation using a specialized
 * AbstractQueuedLongSynchronizer to manage cross-thread signaling.
 * <p>
 * Note that items that are "borrowed" from the bag are not actually
 * removed from any collection, so garbage collection will not occur
 * even if the reference is abandoned.  Thus care must be taken to
 * "requite" borrowed objects otherwise a memory leak will result.  Only
 * the "remove" method can completely remove an object from the bag.
 *
 * @param <BagEntry> the templated type to store in the bag
 */
@Slf4j
public class Bag<BagEntry extends IBagEntry> implements AutoCloseable {


   private final CopyOnWriteArrayList<BagEntry> sharedBagEntryList;
   private final boolean weakThreadLocals;

   private final ThreadLocal<List<Object>> threadList;
   private final IBagStateListener bagStateListener;
   private final AtomicInteger waiters;
   /**
    * this is hand off queue
    */
   private final SynchronousQueue<BagEntry> bagEntryQueue;
   private volatile boolean closed;


   public Bag(final IBagStateListener bagStateListener) {
      this.bagStateListener = bagStateListener;
      this.weakThreadLocals = useWeakThreadLocals();

      this.bagEntryQueue = new SynchronousQueue<>(true);
      this.waiters = new AtomicInteger();
      this.sharedBagEntryList = new CopyOnWriteArrayList<>();
      if (weakThreadLocals) {
         this.threadList = ThreadLocal.withInitial(() -> new ArrayList<>(16));
      } else {
         this.threadList = ThreadLocal.withInitial(() -> new LastElementList<>(IBagEntry.class, 16));
      }
   }

   /**
    * The method will borrow a BagEntry from the bag, blocking for the
    * specified timeout if none are available.
    *
    * @param timeout  how long to wait before giving up, in units of unit
    * @param timeUnit a <code>TimeUnit</code> determining how to interpret the timeout parameter
    * @return a borrowed instance from the bag or null if a timeout occurs
    * @throws InterruptedException if interrupted while waiting
    */
   public BagEntry borrow(long timeout, final TimeUnit timeUnit) throws InterruptedException {
      // Try the thread-local list first
      final List<Object> list = threadList.get();
      for (int i = list.size() - 1; i >= 0; i--) {
         final Object entry = list.remove(i);
         @SuppressWarnings("unchecked") final BagEntry bagEntry = weakThreadLocals ? ((WeakReference<BagEntry>) entry).get() : (BagEntry) entry;
         if (bagEntry != null && bagEntry.compareAndSet(state_not_used, state_using)) {
            return bagEntry;
         }
      }

      // Otherwise, scan the shared list ... then poll the handoff queue
      final int waiting = waiters.incrementAndGet();
      try {
         for (BagEntry bagEntry : sharedBagEntryList) {
            if (bagEntry.compareAndSet(state_not_used, state_using)) {
               // If we may have stolen another waiter's proxyConnection, request another bag add.
               if (waiting > 1) {
                  bagStateListener.addBagItem(waiting - 1);
               }
               return bagEntry;
            }
         }

         bagStateListener.addBagItem(waiting);

         timeout = timeUnit.toNanos(timeout);
         do {
            final long start = currentTime();
            final BagEntry bagEntry = bagEntryQueue.poll(timeout, NANOSECONDS);
            if (bagEntry == null || bagEntry.compareAndSet(state_not_used, state_using)) {
               return bagEntry;
            }

            timeout -= elapsedNanos(start);
         } while (timeout > 10_000);

         return null;
      } finally {
         waiters.decrementAndGet();
      }
   }

   /**
    * This method will return a borrowed object to the bag.  Objects
    * that are borrowed from the bag but never "requited" will result
    * in a memory leak.
    *
    * @param bagEntry the value to return to the bag
    * @throws NullPointerException  if value is null
    * @throws IllegalStateException if the bagEntry was not borrowed from the bag
    */
   public void requite(final BagEntry bagEntry) {
      bagEntry.setState(state_not_used);

      for (int i = 0; waiters.get() > 0; i++) {
         if (bagEntry.getState() != state_not_used || bagEntryQueue.offer(bagEntry)) {
            return;
         } else if ((i & 0xff) == 0xff) {
            parkNanos(MICROSECONDS.toNanos(10));
         } else {
            yield();
         }
      }

      final List<Object> threadLocalList = threadList.get();
      if (threadLocalList.size() < 50) {
         threadLocalList.add(weakThreadLocals ? new WeakReference<>(bagEntry) : bagEntry);
      }
   }

   /**
    * Add a new object to the bag for others to borrow.
    *
    * @param bagEntry an object to add to the bag
    */
   public void add(final BagEntry bagEntry) {
      if (closed) {
         log.info("Bag has been closed, ignoring add()");
         throw new IllegalStateException("Bag has been closed, ignoring add()");
      }

      sharedBagEntryList.add(bagEntry);

      // spin until a thread takes it or none are waiting
      while (waiters.get() > 0 && bagEntry.getState() == state_not_used && !bagEntryQueue.offer(bagEntry)) {
         yield();
      }
   }

   /**
    * Remove a value from the bag.  This method should only be called
    * with objects obtained by <code>borrow(long, TimeUnit)</code> or <code>reserve(BagEntry)</code>
    *
    * @param bagEntry the value to remove
    * @return true if the entry was removed, false otherwise
    * @throws IllegalStateException if an attempt is made to remove an object
    *                               from the bag that was not borrowed or reserved first
    */
   public boolean remove(final BagEntry bagEntry) {
      if (!bagEntry.compareAndSet(state_using, state_removed) && !bagEntry.compareAndSet(state_reserved, state_removed) && !closed) {
         log.warn("Attempt to remove an object from the bag that was not borrowed or reserved: {}", bagEntry);
         return false;
      }

      final boolean removed = sharedBagEntryList.remove(bagEntry);
      if (!removed && !closed) {
         log.warn("Attempt to remove an object from the bag that does not exist: {}", bagEntry);
      }

      return removed;
   }

   /**
    * Close the bag to further adds.
    */
   @Override
   public void close() {
      closed = true;
   }

   /**
    * This method provides a "snapshot" in time of the BagEntry
    * items in the bag in the specified state.  It does not "lock"
    * or reserve items in any way.  Call <code>reserve(BagEntry)</code>
    * on items in list before performing any action on them.
    *
    * @param state one of the {@link IBagEntry} states
    * @return a possibly empty list of objects having the state specified
    */
   public List<BagEntry> values(final int state) {
      final List<BagEntry> bagEntryList = sharedBagEntryList.stream().filter(e -> e.getState() == state).collect(Collectors.toList());
      Collections.reverse(bagEntryList);
      return bagEntryList;
   }

   /**
    * This method provides a "snapshot" in time of the bag items.  It
    * does not "lock" or reserve items in any way.  Call <code>reserve(BagEntry)</code>
    * on items in the list, or understand the concurrency implications of
    * modifying items, before performing any action on them.
    *
    * @return a possibly empty list of (all) bag items
    */
   @SuppressWarnings("unchecked")
   public List<BagEntry> values() {
      return (List<BagEntry>) sharedBagEntryList.clone();
   }

   /**
    * The method is used to make an item in the bag "unavailable" for
    * borrowing.  It is primarily used when wanting to operate on items
    * returned by the <code>values(int)</code> method.  Items that are
    * reserved can be removed from the bag via <code>remove(BagEntry)</code>
    * without the need to unReserve them.  Items that are not removed
    * from the bag can be make available for borrowing again by calling
    * the <code>unReserve(BagEntry)</code> method.
    *
    * @param bagEntry the item to reserve
    * @return true if the item was able to be reserved, false otherwise
    */
   public boolean reserve(final BagEntry bagEntry) {
      return bagEntry.compareAndSet(state_not_used, state_reserved);
   }

   /**
    * This method is used to make an item reserved via <code>reserve(BagEntry)</code>
    * available again for borrowing.
    *
    * @param bagEntry the item to unReserve
    */
   public void unReserve(final BagEntry bagEntry) {
      if (bagEntry.compareAndSet(state_reserved, state_not_used)) {
         // spin until a thread takes it or none are waiting
         while (waiters.get() > 0 && !bagEntryQueue.offer(bagEntry)) {
            yield();
         }
      } else {
         log.warn("Attempt to relinquish an object to the bag that was not reserved: {}", bagEntry);
      }
   }

   /**
    * Get the number of threads pending (waiting) for an item from the
    * bag to become available.
    *
    * @return the number of threads waiting for items from the bag
    */
   public int getWaitingThreadCount() {
      return waiters.get();
   }

   /**
    * Get a count of the number of items in the specified state at the time of this call.
    *
    * @param state the state of the items to count
    * @return a count of how many items in the bag are in the specified state
    */
   public int getCount(final int state) {
      int count = 0;
      for (IBagEntry bagEntry : sharedBagEntryList) {
         if (bagEntry.getState() == state) {
            count++;
         }
      }
      return count;
   }

   public int[] getStateCounts() {
      final int[] states = new int[6];
      for (IBagEntry bagEntry : sharedBagEntryList) {
         ++states[bagEntry.getState()];
      }
      states[4] = sharedBagEntryList.size();
      states[5] = waiters.get();

      return states;
   }

   /**
    * Get the total number of items in the bag.
    *
    * @return the number of items in the bag
    */
   public int size() {
      return sharedBagEntryList.size();
   }

   public void dumpState() {
      sharedBagEntryList.forEach(entry -> log.info(entry.toString()));
   }

   /**
    * Determine whether to use WeakReferences based on whether there is a
    * custom ClassLoader implementation sitting between this class and the
    * System ClassLoader.
    *
    * @return true if we should use WeakReferences in our ThreadLocals, false otherwise
    */
   private boolean useWeakThreadLocals() {
      try {
         if (System.getProperty("com.zaxxer.hikari.useWeakReferences") != null) {   // undocumented manual override of WeakReference behavior
            return Boolean.getBoolean("com.zaxxer.hikari.useWeakReferences");
         }

         return getClass().getClassLoader() != ClassLoader.getSystemClassLoader();
      } catch (SecurityException se) {
         return true;
      }
   }

   public interface IBagStateListener {
      void addBagItem(int waiting);
   }
}
