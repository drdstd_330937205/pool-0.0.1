package karma.pool.pool;

class ConnectionSetupException extends Exception {
   private static final long serialVersionUID = 929872118275916521L;

   ConnectionSetupException(Throwable t) {
      super(t);
   }
}
