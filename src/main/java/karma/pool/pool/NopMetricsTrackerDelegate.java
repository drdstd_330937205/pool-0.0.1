package karma.pool.pool;

/**
 * A no-op implementation of the RecorderDelegate that is used when log capture is
 * disabled.
 */
final class NopMetricsTrackerDelegate implements RecorderDelegate {
}
