package karma.pool.pool;

import org.slf4j.LoggerFactory;

import java.util.concurrent.Executor;

/**
 * Special executor used only to work around a MySQL issue that has not been addressed.
 * MySQL issue: http://bugs.mysql.com/bug.php?id=75615
 */
class SynchronousExecutor implements Executor {

   @Override
   public void execute(Runnable runnable) {
      try {
         runnable.run();
      } catch (Exception t) {
         LoggerFactory.getLogger(AbstractPool.class).debug("Failed to execute: {}", runnable, t);
      }
   }
}
