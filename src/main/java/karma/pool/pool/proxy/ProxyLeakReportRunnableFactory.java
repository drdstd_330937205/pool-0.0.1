package karma.pool.pool.proxy;

import karma.pool.pool.PoolEntry;

import java.util.concurrent.ScheduledExecutorService;


public class ProxyLeakReportRunnableFactory {
   private ScheduledExecutorService scheduledExecutorService;
   private long leakDetectionThreshold;

   public ProxyLeakReportRunnableFactory(final long leakDetectionThreshold, final ScheduledExecutorService scheduledExecutorService) {
      this.scheduledExecutorService = scheduledExecutorService;
      this.leakDetectionThreshold = leakDetectionThreshold;
   }

   public void updateLeakDetectionThreshold(final long leakDetectionThreshold) {
      this.leakDetectionThreshold = leakDetectionThreshold;
   }

   public ProxyLeakReportRunnable schedule(final PoolEntry poolEntry) {
      return (leakDetectionThreshold == 0) ? ProxyLeakReportRunnable.NO_LEAK : bindProxyLeakReportRunnable(poolEntry);
   }

   private ProxyLeakReportRunnable bindProxyLeakReportRunnable(PoolEntry poolEntry) {
      ProxyLeakReportRunnable proxyLeakReportRunnable = new ProxyLeakReportRunnable(poolEntry);
      proxyLeakReportRunnable.scheduleAt(scheduledExecutorService, leakDetectionThreshold);

      return proxyLeakReportRunnable;
   }
}
