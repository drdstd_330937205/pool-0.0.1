package karma.pool.pool.proxy;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class ProxyStatement implements Statement {
   protected final ProxyConnection proxyConnection;
   final Statement statement;
   private boolean isClosed;
   private ResultSet proxyResultSet;

   ProxyStatement(ProxyConnection proxyConnection, Statement statement) {
      this.proxyConnection = proxyConnection;
      this.statement = statement;
   }


   @Override
   public final void close() throws SQLException {
      synchronized (this) {
         if (isClosed) {
            return;
         }

         isClosed = true;
      }

      proxyConnection.removeStatement(statement);

      try {
         statement.close();
      } catch (SQLException e) {
         throw proxyConnection.checkException(e);
      }
   }

   @Override
   public Connection getConnection() throws SQLException {
      return proxyConnection;
   }

   @Override
   public boolean execute(String sql) throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return statement.execute(sql);
   }


   @Override
   public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return statement.execute(sql, autoGeneratedKeys);
   }

   @Override
   public ResultSet executeQuery(String sql) throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      ResultSet resultSet = statement.executeQuery(sql);
      return ProxyFactory.getProxyResultSet(proxyConnection, this, resultSet);
   }

   @Override
   public int executeUpdate(String sql) throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return statement.executeUpdate(sql);
   }


   @Override
   public int[] executeBatch() throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return statement.executeBatch();
   }


   @Override
   public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return statement.executeUpdate(sql, autoGeneratedKeys);
   }


   @Override
   public long[] executeLargeBatch() throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return statement.executeLargeBatch();
   }


   @Override
   public long executeLargeUpdate(String sql) throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return statement.executeLargeUpdate(sql);
   }

   @Override
   public long executeLargeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return statement.executeLargeUpdate(sql, autoGeneratedKeys);
   }


   @Override
   public ResultSet getResultSet() throws SQLException {
      final ResultSet resultSet = statement.getResultSet();
      if (resultSet != null) {
         if (proxyResultSet == null || ((ProxyResultSet) proxyResultSet).resultSet != resultSet) {
            proxyResultSet = ProxyFactory.getProxyResultSet(proxyConnection, this, resultSet);
         }
      } else {
         proxyResultSet = null;
      }
      return proxyResultSet;
   }

   @Override
   public ResultSet getGeneratedKeys() throws SQLException {
      ResultSet resultSet = statement.getGeneratedKeys();
      if (proxyResultSet == null || ((ProxyResultSet) proxyResultSet).resultSet != resultSet) {
         proxyResultSet = ProxyFactory.getProxyResultSet(proxyConnection, this, resultSet);
      }
      return proxyResultSet;
   }

   @Override
   @SuppressWarnings("unchecked")
   public final <T> T unwrap(Class<T> iface) throws SQLException {
      if (iface.isInstance(statement)) {
         return (T) statement;
      } else if (statement != null) {
         return statement.unwrap(iface);
      }

      throw new SQLException("Wrapped proxyStatement is not an instance of " + iface);
   }
}
