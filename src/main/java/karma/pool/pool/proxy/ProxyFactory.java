package karma.pool.pool.proxy;

import karma.pool.pool.PoolEntry;

import java.sql.*;
import java.util.List;

/**
 * Body is replaced (injected) by JavassistProxyFactory
 */
public final class ProxyFactory {
   private ProxyFactory() {

   }


   public static ProxyConnection getProxyConnection(final PoolEntry poolEntry, final Connection connection, final List<Statement> statementList, final ProxyLeakReportRunnable proxyLeakReportRunnable, final long now, final boolean isReadOnly, final boolean isAutoCommit) {
      throw new IllegalStateException("You need to run the CLI build and you need target/classes in your classpath to run.");
   }

   static Statement getProxyStatement(final ProxyConnection proxyConnection, final Statement statement) {

      throw new IllegalStateException("You need to run the CLI build and you need target/classes in your classpath to run.");
   }

   static CallableStatement getProxyCallableStatement(final ProxyConnection proxyConnection, final CallableStatement statement) {

      throw new IllegalStateException("You need to run the CLI build and you need target/classes in your classpath to run.");
   }

   static PreparedStatement getProxyPreparedStatement(final ProxyConnection proxyConnection, final PreparedStatement statement) {

      throw new IllegalStateException("You need to run the CLI build and you need target/classes in your classpath to run.");
   }

   static ResultSet getProxyResultSet(final ProxyConnection proxyConnection, final ProxyStatement proxyStatement, final ResultSet resultSet) {

      throw new IllegalStateException("You need to run the CLI build and you need target/classes in your classpath to run.");
   }
}
