package karma.pool.pool.proxy;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This is the proxy class for java.sql.ResultSet.
 */
public abstract class ProxyResultSet implements ResultSet {
   protected final ProxyConnection proxyConnection;
   protected final ProxyStatement proxyStatement;
   final ResultSet resultSet;//真实的

   protected ProxyResultSet(ProxyConnection proxyConnection, ProxyStatement proxyStatement, ResultSet resultSet) {
      this.proxyConnection = proxyConnection;
      this.proxyStatement = proxyStatement;
      this.resultSet = resultSet;
   }


   @Override
   public final Statement getStatement() throws SQLException {
      return proxyStatement;
   }


   @Override
   @SuppressWarnings("unchecked")
   public final <T> T unwrap(Class<T> iface) throws SQLException {
      if (iface.isInstance(resultSet)) {
         return (T) resultSet;
      } else if (resultSet != null) {
         return resultSet.unwrap(iface);
      }

      throw new SQLException("Wrapped ResultSet is not an instance of " + iface);
   }
}
