package karma.pool.pool.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

final class ClosedConnection {
   static final Connection CLOSED_CONNECTION = getClosedConnection();

   private static Connection getClosedConnection() {
      InvocationHandler invocationHandler = (proxy, method, args) -> {
         final String methodName = method.getName();
         if ("isClosed".equals(methodName)) {
            return Boolean.TRUE;
         } else if ("isValid".equals(methodName)) {
            return Boolean.FALSE;
         }
         if ("abort".equals(methodName)) {
            return Void.TYPE;
         }
         if ("close".equals(methodName)) {
            return Void.TYPE;
         } else if ("toString".equals(methodName)) {
            return ClosedConnection.class.getCanonicalName();
         }

         throw new SQLException("Connection is closed");
      };

      return (Connection) Proxy.newProxyInstance(Connection.class.getClassLoader(), new Class[]{Connection.class}, invocationHandler);
   }
}
