package karma.pool.pool.proxy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class ProxyPreparedStatement extends ProxyStatement implements PreparedStatement {
   ProxyPreparedStatement(ProxyConnection connection, PreparedStatement statement) {
      super(connection, statement);
   }

   @Override
   public boolean execute() throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return ((PreparedStatement) statement).execute();
   }


   @Override
   public ResultSet executeQuery() throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      ResultSet resultSet = ((PreparedStatement) statement).executeQuery();
      return ProxyFactory.getProxyResultSet(proxyConnection, this, resultSet);
   }


   @Override
   public int executeUpdate() throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return ((PreparedStatement) statement).executeUpdate();
   }


   @Override
   public long executeLargeUpdate() throws SQLException {
      proxyConnection.markCommitStateAsDirty();
      return ((PreparedStatement) statement).executeLargeUpdate();
   }
}
