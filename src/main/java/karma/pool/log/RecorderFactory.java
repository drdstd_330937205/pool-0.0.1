package karma.pool.log;

public interface RecorderFactory {
   /**
    * Create an instance of an Recorder.
    *
    * @param poolName  the name of the pool
    * @param poolStats a PoolStats instance to use
    * @return a Recorder implementation instance
    */
   Recorder create(String poolName, PoolStats poolStats);
}
